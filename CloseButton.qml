import QtQuick 2.4

Rectangle {
    width: units * 6
    height: units * 6
	color: "transparent"
    property real crossSize: units * 3.9
    signal clicked()
	Rectangle {
		height: crossSize / 6
		width: parent.crossSize
		anchors.centerIn: parent
		rotation: 45
		transformOrigin: Item.Center
        color: "lightgrey"
	}
    Rectangle {
		height: crossSize / 6
        width: parent.crossSize
        anchors.centerIn: parent
        rotation: -45
        transformOrigin: Item.Center
        color: "lightgrey"
    }
    MouseArea {
        anchors.fill: parent
        onClicked: parent.clicked()
    }
}

