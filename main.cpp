#include <QGuiApplication>
#include <QScreen>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>

#include "languagedb.h"
#ifdef Q_OS_IOS
#include "CAudio.h"
#endif

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
	app.setOrganizationName("ondrejandrej");
	app.setOrganizationDomain("cuteprojects.com");
    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();


	qreal pxPermm = qApp->primaryScreen()->physicalDotsPerInch() / 25.4;

	if(qApp->primaryScreen()->physicalSize().height() > 120.0)	// iPad etc.
	{
		pxPermm = pxPermm * 1.3;
	}
	context->setContextProperty("units", pxPermm);

#ifdef QT_DEBUG
    context->setContextProperty("isDebug", QVariant(true));
#else
    context->setContextProperty("isDebug", QVariant(false));
#endif

    qmlRegisterType<LanguageDB>("custom", 1, 0, "LanguageDB");

#ifdef Q_OS_IOS
	qmlRegisterType<CAudio>("ios_audio", 1, 0, "Audio");
#else
	qmlRegisterType<QObject>("ios_audio", 1, 0, "DummyObject");		// import nothing if not in iOS
#endif

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}
