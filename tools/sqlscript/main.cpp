
#include <QtSql>
#include <QFile>
#include <QtDebug>

void createSQLiteDB();
qint64 last_insert_rowid();
bool countOccurences(QMap<QChar, int> &occur, QString word);
float scoreFromFreq(float freq);

int main(int argc, char *argv[])
{
    if(argc != 3) {
        qDebug() << "usage:\n" << argv[0] << " <language name> <file name>\n ";
        qDebug() << "Inserts new language into languages.db and imports its word list from text file.\n ";
        return 0;
    }
    QString language_name(argv[1]);
    QString text_file_name(argv[2]);

    try {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("../../android/assets/languages.db");
        if(!db.open()) {
            throw QString("SQLite error: failed to create DB file.");
        }

        QSqlQuery query;
        query.exec("PRAGMA foreign_keys = ON");
        query.exec("PRAGMA journal_mode = OFF");
        query.exec("PRAGMA synchronous = OFF");
        createSQLiteDB();

        QFile textFile(text_file_name);
        if(!textFile.open(QIODevice::ReadOnly | QIODevice::Text))
            throw QString("File could not be opened.");

        if(!query.exec("INSERT INTO languages(name) VALUES ('" + language_name + "')"))
            throw query.lastError().text();
        qint64 language_id = last_insert_rowid();

        QSqlQuery insertQuery;
        insertQuery.prepare("INSERT INTO words(word, language_id) VALUES (:word, :language_id)");

        QTextStream textStream(&textFile);
        textStream.setCodec("UTF-8");

        QMap<QChar, int> letters;

        QString line;
        while(true) {
            line = textStream.readLine().trimmed().toUpper();
            if(line.isNull())       // end of file
                break;
            line = line.split("/").at(0);
            if(!countOccurences(letters, line))
                continue;
            if(line.length() < 3)
                continue;
            insertQuery.bindValue(":word", line);
            insertQuery.bindValue(":language_id", language_id);
            if(!insertQuery.exec())
                continue;
//                throw QString("insert failed. Line: " + line + ", language_id: "+ QString::number(language_id))
//                    + QString("\n" + insertQuery.lastError().text());
        }

        //qDebug() << letters;
        float total = 0;
        foreach(const QChar c, letters.keys()) {
            total += letters.value(c);
        }
        query.prepare("INSERT INTO letters(letter, frequency, score, language_id) "
                      "VALUES(:letter, :frequency, :score, :language_id)");
        foreach(const QChar c, letters.keys()) {
            float frequency = static_cast<float>(letters.value(c)) / total;
            if(frequency < 0.0001f)     // exclude e. g. "É" in English
                continue;
            query.bindValue(":letter", c);
            query.bindValue(":frequency", frequency);
            query.bindValue(":score", scoreFromFreq(frequency));
            query.bindValue(":language_id", language_id);
            if(!query.exec())
                throw query.lastError().text();

           // qDebug() << "letter: " << c << ", weight: " << static_cast<float>(letters.value(c)) / total;
        }

    }   // try
    catch(QString e) {
        qDebug() << e;
    }
}


float scoreFromFreq(float freq) {
	return 0.1f/freq;
}

bool countOccurences(QMap<QChar, int> &occur, QString word) {
    foreach(const QChar c, word) {
        if(!c.isLetter())
            return false;
    }
    int val;
    foreach(const QChar c, word) {
        val = occur.value(c, 0);
        val ++;
        occur.insert(c, val);
    }
    return true;
}

void createSQLiteDB() {
    QSqlQuery q;
    if(q.exec("SELECT * FROM languages"))      // if table exists, don't continue
        return;

    q.exec("CREATE TABLE languages ( "
           "language_id INTEGER NOT NULL    PRIMARY KEY, "
           "name TEXT   NOT NULL    UNIQUE)");

    q.exec("CREATE TABLE words ( "
           "word TEXT   NOT NULL, "
           "language_id INTEGER NOT NULL    REFERENCES languages(language_id)   ON DELETE CASCADE,"
           "PRIMARY KEY(word, language_id)"
           ") WITHOUT ROWID");

    q.exec("CREATE TABLE letters ( "
           "letter TEXT NOT NULL, "
           "frequency REAL NOT NULL, "
           "score REAL  NOT NULL, "
           "language_id INTEGER NOT NULL    REFERENCES languages(language_id)   ON DELETE CASCADE)");

    q.exec("CREATE INDEX idx_letters ON letters(letter)");
}

qint64 last_insert_rowid() {
    QSqlQuery q;
    q.exec("SELECT last_insert_rowid()");
    q.next();
    return q.value(0).toLongLong();
}
