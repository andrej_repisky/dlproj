#ifndef LANGUAGEDB_H
#define LANGUAGEDB_H

#include <QObject>
#include <QVariant>

class LanguageDB : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int languageId READ languageId WRITE setLanguageId NOTIFY languageIdChanged)

    Q_PROPERTY(QVariant letterFreqs READ letterFreqs NOTIFY languageIdChanged)
    Q_PROPERTY(QVariant letterScores READ letterScores NOTIFY languageIdChanged)

public:
    explicit LanguageDB(QObject *parent = 0);

    int languageId() const { return m_language_id; }
    void setLanguageId(int val);
    Q_INVOKABLE QVariant availableLang() const;
    QVariant letterFreqs() const { return QVariant(m_letterFreqs); }
    QVariant letterScores() const { return QVariant(m_letterScores); }
    Q_INVOKABLE bool isValid(QString word);

signals:
    void languageIdChanged();

private:
    int m_language_id;
    QVariantMap m_letterScores;
    QVariantMap m_letterFreqs;
};

#endif // LANGUAGEDB_H
