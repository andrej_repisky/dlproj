# Dropping Letters #

Dropping Letters is a mobile game, which tests your ability to combine characters into words quickly. It's available on Google Play: https://play.google.com/store/apps/details?id=org.qtproject.droppingletters

You can exercise your vocabulary in English, German, Esperanto, Slovak and Czech language.

The code of the original Dropping letters app for Ubuntu can be found here:
https://launchpad.net/dropping-letters


![](doc/menu.jpg "Start screen")
  
![](doc/hra.jpg "Gameplay screen")


License: EUPL (see licence.txt)
