#include "languagedb.h"

#include <QtSql>

LanguageDB::LanguageDB(QObject *parent) :
    QObject(parent) {

#ifdef Q_OS_ANDROID
    QString dbFilePath = "assets:/languages.db";
#else
    QString dbFilePath = "../dlproj/android/assets/languages.db";
#endif

    QFile dbFile(dbFilePath);
    if(dbFile.exists()) {
        qDebug() << "copy: " << dbFile.copy("./languages.db");
        QFile::setPermissions("./languages.db", QFile::WriteOwner | QFile::ReadOwner);
    }

//    qDebug() << QDir().currentPath();
//    qDebug() << QDir().entryList();

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("./languages.db");
    db.setConnectOptions("QSQLITE_OPEN_READONLY");
    if(!db.open())
        qCritical() << "failed to open language database";

    QSqlQuery query;
    query.exec("SELECT language_id FROM languages LIMIT 1");
    if(!query.next())
        qCritical() << "failed to get a valid language ID";
    setLanguageId(query.value(0).toInt());
}

void LanguageDB::setLanguageId(int val) {
    if(m_language_id == val)
        return;
    m_language_id = val;

    // update letter frequencies
    QSqlQuery query;
    m_letterFreqs.clear();
    query.exec(QString("SELECT letter, frequency FROM letters WHERE language_id = %1").arg(m_language_id));
    while(query.next()) {
        m_letterFreqs.insert(query.value(0).toString(), query.value(1));
    }

    //update letter scores
    m_letterScores.clear();
    query.exec(QString("SELECT letter, score FROM letters WHERE language_id = %1").arg(m_language_id));
    while(query.next()) {
        m_letterScores.insert(query.value(0).toString(), query.value(1));
    }

    emit languageIdChanged();
}

QVariant LanguageDB::availableLang() const {
    QSqlQuery query;
    QVariantList retVal;
    query.exec("SELECT name, language_id FROM languages ORDER BY name");
    while(query.next()) {
        QVariantMap entry;
        entry.insert("text", query.value(0));
        entry.insert("language_id", query.value(1));
        retVal.append(entry);
    }
    return QVariant(retVal);
}

bool LanguageDB::isValid(QString word)
{
    QSqlQuery query;
    query.exec(QString("SELECT * FROM words "
                       "WHERE word = '%1' AND language_id = %2").arg(word).arg(m_language_id));
	return query.next();
}
