TEMPLATE = app

QT += qml quick multimedia sql

ios {
    QT -= widgets

    QMAKE_INFO_PLIST = ios/Info.plist

    BUNDLE_DATA.files = $$files($$PWD/android/assets/languages.db)    \
	$$files($$PWD/ios/*.aac) \
	$$files($$PWD/ios/Launch.xib) \
	$$files($$PWD/ios/*.png)

    QMAKE_BUNDLE_DATA += BUNDLE_DATA

    HEADERS += CAudio.h
    OBJECTIVE_SOURCES += CAudio.mm
}

SOURCES += main.cpp \
	languagedb.cpp

RESOURCES += qml.qrc \
#	translations.qrc

TRANSLATIONS = resources/translations/dlproj_de.ts \
			   resources/translations/dlproj_eo.ts

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += \
    main.qml \
    android/AndroidManifest.xml \
    Front.qml \
    Back.qml

HEADERS += \
    languagedb.h

DISTFILES += \
    android/assets/languages.db \
    CloseButton.qml \
    Bubble.qml \
    CSeparator.qml
