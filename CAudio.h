#pragma once

#include <QObject>

class CAudio : public QObject
{
	Q_OBJECT

	Q_PROPERTY(QString source READ source WRITE setSource)
	Q_PROPERTY(bool muted READ muted WRITE setMuted)

public:
	explicit CAudio(QObject *parent = 0) : QObject(parent) {}

	QString source() const { return m_source; }
	void setSource(const QString &value) { m_source = value; }
	bool muted() const { return m_muted; }
	void setMuted(const bool &value) { m_muted = value; }

	Q_INVOKABLE void play() const;

private:
	QString m_source;
	bool m_muted;
};

