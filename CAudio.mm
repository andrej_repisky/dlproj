
#include "CAudio.h"

#import <AVFoundation/AVFoundation.h>
#include <QDebug>


void CAudio::play() const
{
	if(m_muted)
		return;

	QStringList splitPath = m_source.split("/");	// extract only filename from the path
	if(splitPath.length() < 2)
		return;
	QString fileName = splitPath.at(1);
	if(fileName.length() < 4)
		return;
	fileName.chop(4);		// remove file extension
	fileName += ".aac";

	NSString *soundFilePath = fileName.toNSString();
	NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
	static AVAudioPlayer *audioPlayer = [AVAudioPlayer alloc];
	[audioPlayer initWithContentsOfURL:soundFileURL error:nil];
	[audioPlayer play];
}

