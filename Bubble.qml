
import QtQuick 2.4

Rectangle {
	id: sheet
	anchors.fill: parent
	color: "#40000000"
	default property alias bubbleContent: bubbleItem.data
	antialiasing: true
	MouseArea {
		anchors.fill: parent
		onClicked: sheet.destroy()
	}
	Rectangle {
		y: helpButton.y + helpButton.height + units * 1
		anchors.right: parent.right
		anchors.rightMargin: units * 2
        width: Math.min(mainWindow.width, units * 55) - units * 5
        height: units * 39
		radius: units * 1

        Item {
            id: bubbleItem
            anchors.fill: parent
            anchors.topMargin: units * 1
        }

        CloseButton {
            anchors.top: parent.top
            anchors.right: parent.right
            onClicked: sheet.destroy()
            color: "#80ffffff"
            crossSize: units * 2.6
            radius: parent.radius
        }

		MouseArea {
			anchors.fill: parent
			z: -1
		}
	}
}

