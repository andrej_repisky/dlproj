import QtQuick 2.4
import QtQuick.LocalStorage 2.0
import QtQuick.Controls 1.2
import QtQuick.Window 2.1
import Qt.labs.settings 1.0
import custom 1.0

Window {
	id: mainWindow
	width: units * 100
	height: units * 110

	visible: true

    Settings {
        id: settings
        property bool musicEnabled: true
        property bool soundsEnabled: true
        property int languageIndex: 0
    }

    LanguageDB {
        id: languageObject
        languageId: availableLang()[settings.languageIndex].language_id
    }

    Flipable {
        id: flipable
        objectName: "flipable"
		property bool flipped: true
        property variant db: null
		anchors.fill: parent

        transform: Rotation {
            id: rotation
            origin.x: flipable.width/2
            origin.y: flipable.height/2
            axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
            angle: 0    // the default angle
        }
        property int minChromeHeight: 50

		Component.onCompleted: {
            var db = LocalStorage.openDatabaseSync("dropping-letters", "1.0", "Dropping Letters", 1000);
            db.transaction(function(tx) {
                // Create the database if it doesn't already exist
                tx.executeSql('CREATE TABLE IF NOT EXISTS Scores(score INT, time TEXT)');
            });
            frontPage.updateScore(0);
			animationTimer.start()
        }
		Timer {		// to play the dropping letters animation
			id: animationTimer
			interval: 0
			onTriggered:
				flipable.flipped = false
		}

        states: [
            State {
                name: "back"
                PropertyChanges { target: rotation; angle: 180 }
                when: flipable.flipped
            },
            State {
                name: "front"
                when: !flipable.flipped
            }
        ]

        transitions: [
            Transition {
                NumberAnimation { target: rotation; property: "angle"; duration: 200 }
            }
        ]

        front: Front {
            id: frontPage
        }

        back: Back {
            id: backPage
        }
    }
}
