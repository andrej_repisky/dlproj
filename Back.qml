import QtQuick 2.4
import QtQuick.Particles 2.0
import QtMultimedia 5.4
import QtQuick.Controls 1.3
import ios_audio 1.0

Rectangle {
    id: main
    color: "#58585A"
    width: flipable.width
    height: flipable.height
    property var selectedItems: []
    property int score: 0
	property int maxHeight: 10

    Keys.onReleased: {
        if(event.key === Qt.Key_Back || event.key === Qt.Key_Escape) {
            flipable.flipped = false
            droptimer.stop()
            frontPage.closeDialogs()
            event.accepted = true
        }
    }

    function resetGame() {
        lm.clear();
        for (var i=0; i<7; i++) {
            lm.append({'letters': [] });
        }
        accum.text = "";
        droptimer.tickCount = 0;
        flipable.flipped = true;
        frontPage.updateScore(main.score);
        main.score = 0;
        droptimer.start();
    }

    function getRandomWeightedLetter() {
        // could work out sumOfWeights once, but this is easier to understand
        var sumOfWeights = 0.0
        for(var k in languageObject.letterFreqs) {
            sumOfWeights += languageObject.letterFreqs[k]
        }
        var selection = Math.random() * sumOfWeights
        for(var k in languageObject.letterFreqs) {
            if(selection < languageObject.letterFreqs[k])  {  return k; }
            selection -= languageObject.letterFreqs[k];
        }
    }

    Audio {
        id: music
        source: "resources/easy-lemon.ogg"
        autoPlay: true
        loops: Audio.Infinite
        muted: !settings.musicEnabled
    }

    Audio {
        id: click
        source: "resources/click-1-off-click.ogg"
		muted: !settings.soundsEnabled
    }

    Audio {
        id: success
        source: "resources/button-chime.ogg"
		muted: !settings.soundsEnabled
	}

    Audio {
        id: failure
        source: "resources/cartoon-bing-low.ogg"
		muted: !settings.soundsEnabled
    }

    Audio {
        id: gameover
        source: "resources/dark-church-bell.ogg"
		muted: !settings.soundsEnabled
    }

	CloseButton {
		anchors.right: parent.right
		anchors.top: parent.top
		z: 2
        onClicked:
            flipable.flipped = false
	}

    Rectangle {
        id: bottombar
        anchors {
            bottom: main.bottom
            left: main.left
            right: main.right
        }
        height: units * 3.9
        color: Qt.hsla(210/360, 0.84, 0.2)

        Item {
            objectName: "mainscore"
            id: mainscore
            anchors {
                centerIn: bottombar
                bottom: bottombar.bottom
            }
            Label {
                anchors.centerIn: parent
                font.pixelSize: units * 3.2
                text: main.score
                color: "white"
            }
        }
    }

    Rectangle {
        id: suggestedWordContainer
        visible: accum.text != ""
        anchors.top: main.top
        anchors.left: main.left
        anchors.right: main.right
        height: units * 7
        z: 2
        color: accum.isValid ? Qt.hsla(124/360, 0.83, 0.45) : Qt.hsla(0, 0.83, 0.65)

        Label {
            id: accum
            objectName: "accumulate"
            anchors.centerIn: parent
            text: ""
            color: "white"
            font.pixelSize: units * 3.9
            font.bold: true
            property bool isValid: false
            onTextChanged: {
                accum.isValid = languageObject.isValid(accum.text);
            }
        }
        function processSuggestedWord() {
            if(accum.isValid) {
                success.play()
                var thisscore = 0, wordlength = accum.text.length
                accum.text = ""

                // tell the boxes to destroy themselves
                main.selectedItems.forEach(function(b) {
                    thisscore += languageObject.letterScores[b.containedLetter]
                    b.state = "dead"
                })
                main.selectedItems = []
                let totalscore = Math.floor(thisscore * wordlength)
                if(totalscore.toString() == "NaN")
                    totalscore = 124
                main.score += totalscore
                scoredisplay.text = totalscore.toString()
                showscoredisplay.start()
            } else {
                failure.play()
                accum.text = ""
                main.selectedItems.forEach(function(b) { b.selected = false; })
                main.selectedItems = []
            }
        }
        focus: true
        Keys.onReturnPressed: {
            suggestedWordContainer.processSuggestedWord();
        }
        MouseArea {
            anchors.fill: parent
            onClicked: { suggestedWordContainer.processSuggestedWord(); }
        }
    }

    Label {
        id: scoredisplay
        objectName: "scoredisplaylabel"
        anchors.centerIn: parent
        z: 3
        font.pixelSize: units * 5
        text: "200"
        color: "red"
        opacity: 0
    }

    ParallelAnimation {
        id: showscoredisplay
        NumberAnimation {
            property: "scale"
            from: 0.1
            to: 8.0
            duration: 400
            target: scoredisplay
        }
        SequentialAnimation {
            NumberAnimation {
                property: "opacity"
                from: 0
                to: 1.0
                duration: 20
                target: scoredisplay
            }
            NumberAnimation {
                property: "opacity"
                from: 1.0
                to: 0
                duration: 380
                target: scoredisplay
            }
        }
    }


    Timer {
        id: droptimer
        objectName: "dropTimer"
        repeat: true
        running: flipable.flipped
        interval: baseInterval
        property int baseInterval: isDebug ? 1000 : 2000
        property int tickCount: 0
        triggeredOnStart: true
        onTriggered: {
            tickCount ++
            droptimer.interval = baseInterval - tickCount * 1
            var idx = Math.round(Math.random() * (lm.count - 1));
            lm.get(idx).letters.append({ letter: main.getRandomWeightedLetter() });
			if(lm.get(idx).letters.count >= main.maxHeight) {
                droptimer.stop()
                gameover.play()
				gameOverTimer.start()
                frontPage.updateScore(main.score)
            }
        }
    }

	Timer {
		id: gameOverTimer
		interval: 1800
		onTriggered: {
			flipable.flipped = false
		}
	}

    ListModel {
        id: lm
    }

    Rectangle {
		id: game
		property int squaresize: Math.min((flipable.width) / 7, (flipable.height - (flipable.minChromeHeight * 2)) / main.maxHeight)
        anchors.top: main.top
        anchors.bottom: bottombar.top
        anchors.left: main.left
        anchors.right: main.right
		scale: -1
        color: "#efefef"
		CSeparator {
			anchors.top: parent.top
			anchors.topMargin: game.squaresize * (main.maxHeight - 1) + 0.5 * units
		}
        Row {
            anchors.horizontalCenter: game.horizontalCenter
            anchors.top: game.top
            anchors.topMargin: 1
            //spacing: 1
            Repeater {
                model: lm
                Column {
                    id: tileColumn
                    property int idx: index
                    width: game.squaresize
                    height: game.height
                    add: Transition {
                        NumberAnimation { properties: "y"; easing.type: Easing.OutBounce; duration: 1000 }
                    }
                    move: Transition {
                        NumberAnimation { properties: "y"; easing.type: Easing.OutBounce }
                    }
                    Repeater {
                        model: letters
                        Rectangle {
							id: box
                            property bool selected: false
                            property int idx: index
                            property string containedLetter: letter
                            color: {
                                if(lm.count == 0)
                                    return "transparent"
								if(lm.get(tileColumn.idx).letters.count >= main.maxHeight) {
                                    return "red";
                                } else if (!selected) {
                                    return Qt.hsla(210/360, 0.84, 0.25 + (idx * 0.05));
                                } else if (accum.isValid) {
                                    return Qt.hsla(124/360, 0.83, 0.45); // "#93cc98";
                                } else {
                                    return Qt.hsla(0, 0.83, 0.65); // "#cc9598"
                                }
							}
							scale: -1
                            width: game.squaresize
                            height: game.squaresize
                            y: game.height + game.squaresize
                            z: 5
                            Label {
                                objectName: "gametilelabel"
                                anchors.centerIn: parent
                                text: letter
                                font.bold: true
                                color: "#ffffff"
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    if (!box.selected) {
                                        box.selected = true
                                        accum.text += letter
                                        click.play()
                                        main.selectedItems[main.selectedItems.length] = box
                                    } else {
                                        if (box === main.selectedItems[main.selectedItems.length - 1]) {
                                            main.selectedItems.pop(main.selectedItems.length - 1);
                                            box.selected = false;
                                            accum.text = accum.text.substr(0, accum.text.length - 1);
                                        }
                                    }
                                }
                            }
                            Behavior on opacity {
                                SequentialAnimation {
                                    ScriptAction { script: pulseEmitter.burst(1000); }
                                    NumberAnimation { properties:"opacity"; duration: 500 }
                                    ScriptAction { script: lm.get(box.parent.idx).letters.remove(box.idx); }
                                }
                            }
                            states: [
                                State { name: "alive" },
                                State {
                                    name: "dead"
                                    PropertyChanges { target: box; opacity: 0 }
                                }
                            ]
                            ParticleSystem {
                                id: particles
                                width: flipable.width / 2
                                height: flipable.width / 2
                                anchors.centerIn: parent
                                clip: false
                                ImageParticle {
                                    source: "resources/redStar.png"
                                    alpha: 0
                                    colorVariation: 0.6
                                }
                                Emitter {
                                    id: pulseEmitter
                                    x: parent.width/2
                                    y: parent.height/2
                                    emitRate: 2000
                                    lifeSpan: 500
                                    enabled: false
                                    velocity: AngleDirection { magnitude: 256; angleVariation: 360; magnitudeVariation: 200; }
                                    size: parent.width / 8
                                    sizeVariation: 8
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
