import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.LocalStorage 2.0

Rectangle {
    color: "#efefea"
	width: flipable.width
	height: flipable.height
    property var currentDialog

    MouseArea {     // block taps from propagating to back page
		enabled: !flipable.flipped
		anchors.fill: parent
	}

    Column {
        id: logo
		anchors.centerIn: parent
        Repeater {
            id: titleRowRepeater
            model: [qsTr("DROP"), qsTr("PING"), qsTr("LETT"), qsTr("ERS ")]
            Row {
				id: titleRow
                property int lineIndex: index
                property string modelString: modelData
                Repeater {
                    id: titleColRepeater
                    model: modelData.split("")
                    Rectangle {
                        id: titleletter
                        width: Math.max(flipable.width * 0.4, flipable.height * 0.4) / titleRowRepeater.count - titleRowRepeater.count * titleRow.spacing
                        height: Math.max(flipable.width * 0.4, flipable.height * 0.4) / titleRowRepeater.count - titleRowRepeater.count * titleRow.spacing
                        color: Qt.hsla(210/360, 0.84, 0.25 + ((4-parent.lineIndex) * 0.05) + ((4-index) * 0.05));
                        Label {
                            objectName: "lettertile"
                            text: modelData
							anchors.centerIn: parent
                            font.bold: true
                            color: "#ffffff"
                        }
                        states: [
							State { name: "showing"; when: flipable.flipped == false
                                PropertyChanges { target: titleletter; y:  0 }
                            },
							State { name: "notshowing"; when: flipable.flipped == true
                                PropertyChanges { target: titleletter; y: -units * (300) }
                            }
						]
						transitions: [
							Transition {
								from: "notshowing"
								to: "showing"
								SequentialAnimation {
									PauseAnimation {
										duration: (35 * parent.modelString.length * (4-parent.lineIndex)) + (
											35 * index) }
									NumberAnimation {
										properties: "y"
										easing.type: Easing.OutQuart
									}
								}
							}
						]
                    }
                }
            }
        }
    }
    Label {
        id: playbutton
        objectName: "playButton"
        text: qsTr("Play")
        anchors.top: logo.bottom
        anchors.topMargin: units * 3
        anchors.horizontalCenter: logo.horizontalCenter
        font.pixelSize: units * 3.9
		color: "#0e59a4"
        MouseArea {
            anchors.fill: parent
            enabled: !flipable.flipped
            onClicked: {
                backPage.resetGame();
            }
        }
    }
    Label {
        id: bestscore
        objectName: "bestscorelabel"
        property int bestsofar: 0
        text: "..."
        anchors.top: playbutton.bottom
        anchors.topMargin: units * 3
        anchors.horizontalCenter: logo.horizontalCenter
        color: "#222222"
    }

    function updateScore(score) {
        var db = LocalStorage.openDatabaseSync("dropping-letters", "1.0", "Dropping Letters", 1000);
        if (score > bestscore.bestsofar) {
            db.transaction(function(tx) {
                tx.executeSql("insert into Scores values (?,?)", [backPage.score, 0]);
            });
            bestscore.bestsofar = score;
        } else if (score === 0) {
            db.transaction(function(tx) {
                var res = tx.executeSql('SELECT score from Scores order by score DESC LIMIT 1');
                if (res.rows.length > 0) {
                    bestscore.bestsofar = res.rows.item(0).score;
                }
            });
        }
        bestscore.text = qsTr("Best score: %1").arg(bestscore.bestsofar)
    }
    Rectangle {
        id: helpButton
        width: units * 8
        height: units * 8
        radius: units * 1.3
        Label {
            text: "?"
            anchors.centerIn: parent
            font.pixelSize: parent.height / 2.5
            font.bold: true
        }
        anchors.top: parent.top
        anchors.right: settingsButton.left
        anchors.topMargin: units * 2
        anchors.rightMargin: units * 1
        MouseArea {
            anchors.fill: parent
            enabled: !flipable.flipped
            onClicked: {
				currentDialog = helpPop.createObject(mainWindow.contentItem)
            }
        }
    }

    function closeDialogs() {
        if((typeof currentDialog) != "undefined" &&
                currentDialog !== null)
            currentDialog.destroy()
    }

    Component {
        id: helpPop
		Bubble {
            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.margins: units * 2
                spacing: units * 1
                Label {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    text: qsTr("Instructions: Tap letter boxes to form a word. Tap the top bar " +
                          "when it turns green to remove the highlighted letters.")
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                }
                Rectangle {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: units * 0.1
                    color: "black"
                }
                Label {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    text: qsTr("Music: %1 from Kevin McCloud<br>").arg("<a href='http://incompetech.com/music/royalty-free/index.html?isrc=USUAN1200076'>Easy Lemon</a>") +
                          qsTr("Sounds: freesound from %1, %2, %3 and %4").arg("<a href='http://www.freesound.org/people/kantouth/sounds/106727/'>kantouth</a>").arg("<a href='http://www.freesound.org/people/tictacshutup/sounds/407/'>tictacshutup</a>").arg("<a href='http://www.freesound.org/people/dj-chronos/sounds/45137/'>dj-chronos</a>").arg("<a href='http://www.freesound.org/people/justinbw/sounds/80921/'>justinbw</a><br>")
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
		}
    }
    Rectangle {
        id: settingsButton
        width: units * 8
        height: units * 8
        radius: units * 1.3
        Image {
            anchors.centerIn: parent
            height: parent.height / 2.5
            width: height
            source: "resources/settings.png"
            antialiasing: true
        }

        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: units * 2
        MouseArea {
            anchors.fill: parent
            enabled: !flipable.flipped
            onClicked: {
				currentDialog = settingsPop.createObject(mainWindow.contentItem)
            }
        }
    }
    Component {
        id: settingsPop
		Bubble {
			id: bubble
			Column {
				anchors.centerIn: parent
				spacing: units * 1
				CheckBox {
					anchors.left: parent.left
					anchors.leftMargin: units * 1
                    text: qsTr("Music")
					checked: settings.musicEnabled
					onCheckedChanged:
						settings.musicEnabled = checked
				}
				CheckBox {
					anchors.left: parent.left
					anchors.leftMargin: units * 1
                    text: qsTr("Sounds")
					checked: settings.soundsEnabled
					onCheckedChanged:
						settings.soundsEnabled = checked
				}
				Rectangle {
					width: Math.max(languageCombo.width, langLabel.width) + units * 2
					height: languageCombo.height + langLabel.height + units * 3.5
					border.color: "#808080"
					border.width: units * 0.1
					radius: units * 0.5
					Label {
						id: langLabel
						anchors.top: parent.top
						anchors.margins: units * 1.1
						anchors.left: parent.left
                        text: qsTr("Language:")
					}
					ComboBox {
						id: languageCombo
						anchors.top: langLabel.bottom
						anchors.left: parent.left
						anchors.margins: units * 1.1
						currentIndex: settings.languageIndex
						model: languageObject.availableLang()
						onCurrentIndexChanged: {
							languageObject.languageId = languageObject.availableLang()[currentIndex].language_id
							settings.languageIndex = currentIndex
						}
					}
				}
			}
		}
    }
}
